@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Мой профиль</h1>
@stop

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (Session::has('success'))
        <div class="alert alert-info">{{ Session::get('success') }}</div>
    @endif
    @if (Session::has('deleted'))
        <div class="alert alert-danger">{{ Session::get('deleted') }}</div>
    @endif
    
    <form action="{{route('changedMyProfile')}}" method="POST">
        {{csrf_field()}}
        <input type="hidden" name="id" value="{{$user->id}}">
        <h4>Имя</h4>
        <input type="text" name="name" value="{{$user->name}}">
        <h4>фамилия</h4>
        <input type="text" name="lastname" value="{{$user->lastname}}"><br><br>
        <input type="submit" class="btn btn-warning" value="Редактировать">
        <a class="btn btn-danger" chref="/myProfile/delete" disabled>Удалить аккаунт</a>
    </form>

@endsection