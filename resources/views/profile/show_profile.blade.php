@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Мой профиль</h1>
@stop

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (Session::has('success'))
        <div class="alert alert-info">{{ Session::get('success') }}</div>
    @endif
    @if (Session::has('deleted'))
        <div class="alert alert-danger">{{ Session::get('deleted') }}</div>
    @endif
    
    <p>Имя: {{$user->name}}</p>
    <p>Фамилия: {{$user->lastname}}</p>
    <p>Eмайл: {{$user->email}}</p>
    <p>Cтатус: {{$user->status}}</p>
@endsection