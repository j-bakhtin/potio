@extends('adminlte::page')

<link rel="stylesheet" href="{{ asset('css/style.css') }}">
@section('title', 'AdminLTE')

@section('content_header')
    {{--<h1>Страница не найдена</h1>--}}
    <div class="header1">Страница не найдена</div>
@stop

@section('content')
    <br>
    <br>
    <br>
    <br>
    <p class="center-e"> <img src="/img/l2.png" alt="PotioLogo" /><br> </p>
    <div class="center-e">   <div class="header1">Error 404</div> <br> Упс, что-то пошло не так.. Но Вы можете нажать на <a href="http://potio/">эту волшебную надпись</a>, чтобы вернуться на главную страницу!</div>
@endsection
