<script>
    function agreeForm() {
        var cb = document.getElementsByTagName('input'), L = cb.length - 1, f = true;
        for (; L >= 0; L--) {
            if (cb[L]['type'] == 'checkbox' && cb[L]['checked'] == true) {
                f = !f;
                break;
            }
        }
        document.getElementById('deleteButton').disabled = f;
    }
</script>

<link rel="stylesheet" href="{{ asset('css/style.css') }}">

@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Мои напитки</h1>
@stop

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (Session::has('success'))
        <div class="alert alert-info">{{ Session::get('success') }}</div>
    @endif
    @if (Session::has('deleted'))
        <div class="alert alert-danger">{{ Session::get('deleted') }}</div>
    @endif

    <form action="{{route('ShowDrinks')}}" method="GET">
        <div class="input-group">
            <input type="text" class="form-control" name="search" placeholder="Поиск">
            <div class="input-group-btn">
                <button class="btn btn-default" type="submit">
                    <i class="glyphicon glyphicon-search"></i>
                </button>
            </div>
        </div>
    </form>

    <table class="table table-striped">
        <thead>
            <th>Имя</th>
            <th>Категория</th>
            <th>Рейтинг</th>
            <th>Количество<br> компонентов</th>
            <th>Энергетическая<br>ценность</th>
            <th>Комментарии</th>
            <th>Дата</th>
            <th>Действия</th>
            <th class="text-center">Выбрать</th>
        </thead>
        <tbody>
            <form id="delete_form" action="{{route('DeleteDrink')}}" id="form1" name="form1" method="POST">
                {{csrf_field()}}
                @forelse ($drinks as $drink)
                <tr>
                    <td>{{$drink->name}}</td>
                    <td>{{$drink->type}}</td>
                    <td>{{$drink->rating_average}}</td>
                    <td>{{count($drink->components)}}</td>
                    <td>{{$drink->energy_value_sum}}</td>
                    <td>{{count($drink->comments)}}</td>
                    <td>{{$drink->created_at->format('Y-m-d')}}</td>
                    <td>
                        <a class="btn btn-sm btn-primary" href="/drinks/{{$drink->id}}">Просмотр</a>
                        <a class="btn btn-sm btn-warning" href="/drinks/{{$drink->id}}/change">Изменить</a>
                    </td>
                    <td align="center"><input name="selected[]" type="checkbox" value="{{$drink->id}}" onclick="agreeForm()"></td>
                </tr>

                @empty
                    <tr>
                        <td colspan="9" class="text-center">
                            <h3>Данные отсутствуют</h3>
                            <a href="/create_drinks">Добавить</a>
                        </td>
                    </tr>
                @endforelse
            </form>
        </tbody>
    </table>
    <div style="display: flex; justify-content: space-between;">
        {{$drinks->links()}}
        <div></div>
        @if(auth()->user()->status == 'admin') 
            <input style="margin: 20px 0;" form="delete_form" class="btn btn-danger" id="deleteButton" type="submit" value="Удалить" disabled>
        @endif
    </div>
@endsection
