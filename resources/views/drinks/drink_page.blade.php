<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="{{ asset('css/style.css') }}">
<link rel="stylesheet" href="{{ asset('css/drink_page.css') }}">

@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>{{$drink->name}}</h1>
@stop

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (Session::has('success'))
        <div class="alert alert-info">{{ Session::get('success') }}</div>
    @endif
    
    <h4>Рецепт</h4>
    <p>{{$drink->recipte}}</p>

    <h4> Описание</h4>
    <p>{{$drink->description}}</p>
    
    @if($photos)
        <h4>Изображения</h4>
        <div class='xm1'>
            <div class='gallery'>
                <ul class='img-list'>
                    @foreach($photos as $photo)
                    <li><a  href='{{asset($photo->src)}}'><img width='200' hight='200' src="{{asset($photo->src)}}"></a></li>
                    @endforeach
                </ul>
                <div class="lightbox">
                    <div class="overlay"></div>
                    <figure>
                        <span class="prev">prev</span>
                            <img src="#">
                        <span class="next">next</span>
                    </figure>
                    <div>
                        <ul class='slider'>
                            @foreach($photos as $photo)
                            <li><a  href='{{asset($photo->src)}}'>1</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    @endif
    
    <h4>Рейтинг<h4>
    @if(Auth::check())
        <div class="rating-container">
            <input type="hidden" name="user_id" id="user_id" value="{{auth()->user()->id}}">
            <input type="hidden" name="drink_id" id="drink_id" value="{{$drink->id}}">
            @for($i = 1; $i <= 5; $i++)
                @if($i <= $drink->rating_user)
                    <input class="input" id="input{{$i}}" name="r{{$i}}" type="radio" value="{{$i}}" checked>
                @else
                    <input class="input" id="input{{$i}}" name="r{{$i}}" type="radio" value="{{$i}}">
                @endif
            @endfor
            <p class="rating_average">
                Рейтинг: <span class="average">{{$drink->rating_average}}</span> 
                Голосов: <span  class="count">{{$drink->rating_count}}</span>
            </p>
        </div>
    @else
        <p class="rating_average">
            Рейтинг: <span class="average">{{$drink->rating_average}}</span> 
            Голосов: <span  class="count">{{$drink->rating_count}}</span>
        </p>
    @endif

    <h4> Комментарии:</h4>
    @if(Auth::check())
        <div class="card">
            <div class="card-header"><b>{{auth()->user()->name}}</b></div>
            <div class="card-body">
                <textarea style="height: 100px;resize: none;" class="form-control" required id="text-comment" cols="20" rows="10"></textarea><br>
                <button type="submit" class="btn btn-primary" id="add-comment">Отправить</button>
            </div>
        </div>
        <br>
    @endif
    
    <div class="comments">
        @forelse($comments as $comment)
            <div class="card comment">
                <div class="card-header">
                    {{$comment->user->name}}
                </div>
                <div class="card-body">
                    {{$comment->text}}
                </div>
                <div class="card-footer">
                    {{$comment->created_at->diffForHumans()}}
                </div>
            </div>
            <br>
        @empty
            <p class="not-comment">Комментариев нет!</p>
        @endforelse
    </div>
    @if(Auth::check())
        <script>
            setParam({{auth()->user()->id}}, {{$drink->id}});
        </script>
    @endif
@endsection