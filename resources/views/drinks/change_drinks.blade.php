<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="{{ asset('css/change_drink.css') }}">

@extends('adminlte::page')

@push('js')
    <script src="{{asset('js/change_drink.js')}}"></script>
    <script>
        setUserParam({{auth()->user()->id}}, {{$drink->id}})
    </script>
@endpush

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Изменение напитака - {{ $drink->name }}</h1>
@stop

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (Session::has('success'))
        <div class="alert alert-info">{{ Session::get('success') }}</div>
    @endif

    <h4>Название</h4>
    <input required id="name" class="form-control" type="text" name="name" value="{{ $drink->name }}">
    <h4>Тип</h4>
    <select id="type" class="form-control" name="type" value="{{ $drink->type }}">
        @foreach ($categories as $category)
            <option>{{ $category }}</option>
        @endforeach
    </select>
    <h4>Рецепт</h4>
    <textarea id="recipte" class="form-control" required name="recipte" cols="30" rows="10">{{ $drink->recipte }}</textarea>
    <h4>Описание</h4>
    <textarea id="description" class="form-control" required name="description" cols="30" rows="10">{{ $drink->description }}</textarea><br>
    <input form="form1" id ="btn-change-fields" class="btn btn-warning btn-add" type="submit" value="Изменить">
    <span class="change-fields-status"></span>

    <h3><b>Изменить / Удалить имеющиеся компоненты в напитке</b></h3>
        <table class="table table-striped fixed_header change-delete-ingredients">
            <thead>
                <th>Наименование</th>
                <th>Энергетическая ценность</th>
                <th>Количество ингредиентов</th>
                <th>Удалить</th>
            </thead>
            <tbody>
            @forelse ($drink->components as $component)
                <tr class='tr_{{$component->ingredient->id}}'>
                    <td>{{$component->ingredient->name}}</td>
                    <td>{{$component->ingredient->energy_value}}</td>
                    <td><input class="amount" name="amount[]" type="number" min="1" value="{{$component->amount}}"></td>
                    <td>
                        <label>
                            <input 
                            class="selected"
                            name="selected[]" 
                            type="checkbox" 
                            value="{{$component->ingredient->id}}"> Выбрать
                        </label>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="4" class="text-center">
                        <p>Данные отсутствуют</p>
                    </td>
                </tr>
            @endforelse
            </tbody>
        </table>
        <input id="btn-change" class="btn btn-warning btn-add" type="submit" value="Изменить">
        <input id="btn-delete" class="btn btn-danger btn-add" type="submit" value="Удалить выбранные">
        <span class="change-delete-ingredients-status"></span><br>
    
    <h3><b>Добавить компоненты в напиток</b></h3>

    <div class="input-group">
        <input id="search-input" type="text" class="form-control" name="search" placeholder="Поиск" autocomplete="off">
    </div><br>

    <table class="table table-striped fixed_header add-ingredients">
        <thead>
            <th>Наименование</th>
            <th>Энергетическая ценность</th>
            <th>Количество ингредиентов</th>
            <th>Добавить</th>
        </thead>
        <tbody>
        </tbody>
    </table>
@endsection