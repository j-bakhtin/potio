<link rel="stylesheet" href="{{ asset('css/style.css') }}">
<script>
    function agreeForm() {
        var cb = document.getElementsByTagName('input'), L = cb.length - 1, f = true;
        for (; L >= 0; L--) {
            if (cb[L]['type'] == 'checkbox' && cb[L]['checked'] == true) {
                f = !f;
                break;
            }
        }
        document.getElementById('deleteButton').disabled = f;
    }

    function changeDrink() {

    }
</script>

@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (Session::has('success'))
        <div class="alert alert-info">{{ Session::get('success') }}</div>
    @endif
    @if (Session::has('deleted'))
        <div class="alert alert-danger">{{ Session::get('deleted') }}</div>
    @endif

    @forelse (array_reverse($drinks->all()) as $drink)
        <div class="card">
            <div class="card-header">
                <b>{{$drink->name}}</b>
            </div>
            <div class="card-body">
                <p class="card-text">{{$drink->description}}</p>
                <p class="card-text">Рейтинг: {{$drink->rating_average}}</p>
                <a href="/drinks/{{$drink->id}}" class="btn btn-primary">К рецепту</a>
            </div>
        </div>
        <br>
    @empty
        <p>Данные отсутствуют</p>
    @endforelse

    {{$drinks->links()}}

@endsection
