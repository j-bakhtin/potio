<link rel="stylesheet" href="{{ asset('css/create_drinks.css') }}">
<script>
   function agreeForm(f) {
    var title = "selected_" + f;
    var title1 = "amount_" + f;
    // Если поставлен флажок, снимаем блокирование кнопки
    if (document.getElementById(title).checked){
        document.getElementById(title1).disabled = 0
        document.getElementById(title1).value = 1
    }
    // В противном случае вновь блокируем кнопку
    else {
      document.getElementById(title1).disabled = 1
      document.getElementById(title1).value = null
    }
   }
</script>

@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Добавить напиток</h1>
@stop

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (Session::has('success'))
        <div class="alert alert-info">{{ Session::get('success') }}</div>
    @endif

    <form action="{{route('CreateDrinks')}}" method="POST" enctype="multipart/form-data">
        {{csrf_field()}}
        <h4>Название</h4>
        <input requared class="form-control" type="text" name="name">

        <h4>Тип</h4>
        <select class="form-control" name="type" id="">
            @foreach ($categories as $category)
                <option>{{ $category }}</option>
            @endforeach
        </select>

        <h4> Выбор ингредиентов </h4>
<!--    <div class="input-group">
            <input type="text" class="form-control" placeholder="Введите название ингредиента, чтобы его найти">
            <div class="input-group-btn">
                <button class="btn btn-default" type="submit">
                    <i class="glyphicon glyphicon-search"></i>
                </button>
            </div>
        </div><br> --> 
        <table style="border: #D2D6DE solid 1px;" class="table table-striped fixed_header" height="100" style="height: 100px; overflow: scroll !Important;">
            <thead>
                <th class="th-sm">Наименование</th>
                <th>Энергетическая ценность</th>
                <th>Количество ингредиентов</th>
                <th>Выбрать</th>
            </thead>
            <tbody>
            @forelse ($ingredients as $ingredient)
                <tr>
                    <td>{{$ingredient->name}}</td>
                    <td>{{$ingredient->energy_value}}</td>
                    <td><input name="amount[]" id="amount_{{ $ingredient->id }}" type="number" disabled></td>
                    <td>
                        <label for="selected_{{ $ingredient->id }}">
                            <input name="selected[]" id="selected_{{ $ingredient->id }}" 
                            type="checkbox" 
                            value="{{$ingredient->id}}" 
                            onclick="agreeForm({{$ingredient->id}})">
                            Выбрать
                        </label> 
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="4" class="text-center">
                        <p>Данные отсутствуют</p>
                        @if(auth()->user()->status == 'admin')
                            <a href="/create_ingredients">Добавить</a>
                        @endif
                    </td>
                </tr>
            @endforelse
            </tbody>
        </table>

        <h4>Рецепт</h4>
        <textarea required class="form-control" name="recipte"></textarea>

        <h4>Описание</h4>
        <textarea required class="form-control" name="description"></textarea>

        <h4>Загрузка фотографий</h4>
        <input type="file" name="photo[]" multiple accept="image/*, image/jpg"><br>
        <input class="btn btn-primary btn-add" type="submit" value="Добавить">
    </form>
@endsection