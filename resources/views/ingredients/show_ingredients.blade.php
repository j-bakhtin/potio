<script>
    function agreeForm() {
        var cb = document.getElementsByTagName('input'), L = cb.length - 1, f = true;
        for (; L >= 0; L--) {
            if (cb[L]['type'] == 'checkbox' && cb[L]['checked'] == true) {
                f = !f;
                break;
            }
        }
        document.getElementById('deleteButton').disabled = f;
    }
</script>

@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Ингредиенты</h1>
@stop

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (Session::has('success'))
        <div class="alert alert-info">{{ Session::get('success') }}</div>
    @elif (Session::has('deleted'))
        <div class="alert alert-danger">{{ Session::get('deleted') }}</div>
    @endif


    <form action="{{route('ShowIngredients')}}" method="GET">
        <div class="input-group">
            <input type="text" class="form-control" name="search" placeholder="Поиск">
            <div class="input-group-btn">
                <button class="btn btn-default" type="submit">
                    <i class="glyphicon glyphicon-search"></i>
                </button>
            </div>
        </div>
    </form>

    <table class="table table-striped">
        <thead>
            <th>Наименование</th>
            <th>Энергетическая ценность</th>
            <th>Дата создания</th>
            @if (auth()->user()->status == 'admin')
                <th>Действия</th>
                <th class="text-center">Удалить</th>
            @endif
        </thead>
        <tbody>
            <form id="delete_form" action="{{route('DeleteIngredient')}}" method="GET">
                @forelse ($ingredients as $ingredient)
                    <tr>               
                        <td>{{$ingredient->name}}</td>
                        <td>{{$ingredient->energy_value}}</td>
                        <td>{{$ingredient->created_at->format('Y-m-d')}}</td>
                        @if (auth()->user()->status == 'admin')
                            <td>
                                <a class="btn btn-sm btn-warning" href="/ingredients/{{$ingredient->id}}/change">Изменить</a>
                            </td>
                            <td align="center"><input name="selected[]" type="checkbox" value="{{$ingredient->id}}" onclick="agreeForm()"></td>
                        @endif
                    </tr>
                @empty
                    <tr>
                        @if(auth()->user()->status == 'admin')
                            <td colspan="5" class="text-center">
                                <p>Данные отсутствуют</p>
                                <a href="/create_ingredients">Добавить</a>
                            </td>
                        @else
                            <td colspan="4" class="text-center">
                                <p>Данные отсутствуют</p>
                            </td>
                        @endif
                    </tr>
                @endforelse
            </form>
        </tbody>
    </table>
    <div style="display: flex; justify-content: space-between;">
        {{$ingredients->links()}}
        <div></div>
        @if(auth()->user()->status == 'admin') 
            <input style="margin: 20px 0;" form="delete_form" class="btn btn-danger" id="deleteButton" type="submit" value="Удалить" disabled>
        @endif
    </div>
@endsection
