@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Изменение ингредиента - {{ $ingredient->name }}</h1>
@stop

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (Session::has('success'))
        <div class="alert alert-info">{{ Session::get('success') }}</div>
    @endif

    <form id="form1" action="{{ route('changeFieldsIngredient', ['id' => $ingredient->id]) }}" method="POST">
        {{csrf_field()}}
        <p>Энергетическая ценность</p>
        <input required class="form-control" type="text" name="energy_value" value="{{ $ingredient->energy_value }}"><br>
        <input form="form1" class="btn btn-warning btn-add" type="submit" value="Изменить"><br>
    </form>
@endsection