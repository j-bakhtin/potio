@extends('adminlte::page')

@section('title', 'AdminLTE')


@section('content_header')
    <h1>Добавить новый ингредиент</h1>
@stop

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (Session::has('success'))
        <div class="alert alert-info">{{ Session::get('success') }}</div>
    @endif

    <form action="{{route('CreateIngredient')}}" method="POST">
        {{csrf_field()}}

        <h4>Имя ингредиента</h4>
        <input required type="text" class="form-control" placeholder="Введите название ингредиента" name="name">
        
        <h4>Энергетическая ценность (на 100 гр.)</h4>
        <input required type="text" class="form-control" placeholder="Введите энергетическую ценность продукта" name="energy_value"><br>

        <input type="submit" class="btn btn-primary" value="Добавить">
    </form>
@endsection