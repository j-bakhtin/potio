<?php

namespace App\Http\Middleware;
use App\Http\Controllers\Auth;
use Illuminate\Support\Facades\Redirect;

use Closure;

class checkStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        switch (auth()->user()->status) {
            case 'admin':
                return $next($request);
                break;

            case 'user':
                return Redirect::Back();
                break;
        
            default:
                return Redirect::Back();
        }
    }
}
