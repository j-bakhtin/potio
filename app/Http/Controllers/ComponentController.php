<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Auth;
use App\Models\Component;
use App\Models\Ingredient;
use App\Models\Drink;
use Illuminate\Http\Request;

class ComponentController extends Controller
{
    // Функция создакт новый компонет напитка
    public static function create($ingredient_id, $drink_id, $amount)
  	{
        $ingredient = Ingredient::findOrFail($ingredient_id);
        $drink = Drink::findOrFail($drink_id);

        if (preg_match('/^[1-9]{1}[0-9]{0,}/', $amount)){
            $component = new Component(['ingredient_id'=>$ingredient->id, 
                'drink_id'=>$drink->id, 
                'amount'=>$amount]);
            $component->save();
            return $component;
        }
  	}

    // Функция удаляет компонеет напитка
    public static function delete($compomemt_id){
          $component = Component::findOrFail($compomemt_id);
          $component->delete();
    }

    // Функция изменяет количество ингредиенотов в компонеете
    public static function changeAmount($compomemt_id, $amount){
        $component = Component::findOrFail($compomemt_id);
        if (preg_match('/^[1-9]{1}[0-9]{0,}/', $amount)){
            $component->amount = $amount;
            $component->save();
        }
        return;
    }
}
