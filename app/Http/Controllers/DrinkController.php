<?php

namespace App\Http\Controllers;

use App\Models\Rating;
use App\Models\Drink;
use App\Models\User;
use App\Models\Component;
use App\Models\Ingredient;
use App\Http\Controllers\Auth;
use App\Http\Controllers\ComponentController;
use App\Http\Controllers\ImageController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class DrinkController extends Controller
{
    //Создание напитка
    public function create(Request $request)
    {
    	//dd($request->all());
        //Валидация данных
        $validatedData = $request->validate([
            'name' => 'required|min:5',
            'type' => 'required',
            'recipte' => 'required',
            'description' => 'required|max:250',
            'selected' => 'required',
            'amount' => 'required'
        ]);

        //Определем id пользователя создавшего напиток
        $request['user_id'] = auth()->user()->id;

        //Добавляем напиток в BD

        $drink = new Drink($request->all());
        $drink->save();

        //Перебо компонентов напитка и добавление в BD
        //selected - массив выбранных компонетов, поле value = id_ингредиента
        //amount - массив, элементы которого содержат информацию о количесте выбранного элемента
        //количесво элементов массива selected равно количесву элементов массива amount
        foreach ($request->selected as $key => $value) {
            ComponentController::create($request->selected[$key], $drink->id, $request->amount[$key]);
        }

        // Добавление фотографий
        $files = $request->file('photo');
        if ($files) {
        	ImageController::save($files, 'Описание', $drink->id);
        }

        $request->session()->flash('success', 'Напиток создан');
        return Redirect::back();
	}

	//Показ пользователю напитков которые он создал
	public function showMy()
	{
		if (request()->has('search')){
			$drinks = Drink::where('user_id', auth()->user()->id)->
			where('name', 'like', '%'.request()->search.'%')->paginate(15);
		}else{
			$drinks = Drink::where('user_id', auth()->user()->id)->paginate(15);
		}
		
		//Категории к которым можно отнести напиток
		$categories = Drink::getCategories();

    	// Перебираем все напитки
    	foreach ($drinks as $drink) {
    		if(count($drink->rating) > 0){
	    		// Сумма всех рейтингов для данного напитка
		    	$rating_summ = 0;
		    	// Количество всех рейтингов
		    	$rating_count = 0;
		    	// Рейтинг пользователя просматривающего напиток
		    	$rating_user = 0;
		    	// Среднее значениерейтинга
		    	$rating_average = 0;
		    	// Подсчитываем для каждого напитка

		    	foreach ($drink->rating as $item) {
		    		$rating_summ += $item->rating;
		    		$rating_count++;
		    		if(auth()->check()){
			    		if($item->user_id == auth()->user()->id){
			    			$rating_user = $item->rating;
			    		}
			    	}
		    	}
		    	//Вычисление среднего значения
	    		$drink['rating_average'] = round($rating_average = $rating_summ / $rating_count, 2);
	    	} else 
	    		$drink['rating_average'] = 0;

	    	$energy_value_sum = 0;
	    	foreach ($drink->components as $component) {
	    		$ingredient = $component->ingredient;
	    		//dd($ingredient->energy_value);
	    		$energy_value_sum += $component->amount * $ingredient->energy_value;
	    	}
	    	$drink['energy_value_sum'] = $energy_value_sum;
	    }
		return view('drinks/show_my_drinks', ['drinks' => $drinks, 'categories'=>$categories]);
	}

	//Отоброжение формы для создания напитка
	public function showFormCreate()
	{
		//Категории к которым можно отнести напиток
		$categories = Drink::getCategories();
		//Ингредиенты 
		$ingredients = Ingredient::get();
		return view('drinks/create_drinks', ['categories' => $categories, 'ingredients' => $ingredients]);
	}

	//Удаление напитка
	public function delete(Request $request)
	{
		//Валидация данных
        $validatedData = $request->validate([
            'selected' => 'required',
        ]);

		//Перебор выбранных напитков
		//Заменить на foreach
		foreach ($request->selected as $id){
			$drink = Drink::findOrFail($id);

			if(auth()->user()->can('delete', $drink)){
				$drink->delete();
			}
		}

        //Фомирование пуш-сообщения 
        $message = 'Напиток удален';
        if (count($request->selected) > 1) {
            $message = 'Напитки удалены';
        }

        // Вывод сообщения
        $request->session()->flash('deleted', $message);
        return Redirect::back();
    }

    // Показать все сообщения в меню "Мои напитка"
	public function showAll()
	{
		$drinks = Drink::paginate(15);

    	foreach ($drinks as $drink) {
    		$drink->countRating();
	    }
		return view('drinks/show_all_drinks', ['drinks' => $drinks]);
	}

	// Функция оттображает форму создания напитка
	public function showFormChange($drink_id)
	{	
		// Получаем напиток
		$drink = Drink::findOrFail($drink_id);
		// Проверяем, евляется ли напиток его
		$this->authorize('change', $drink);

		// Может ли данный пользователь редактировать данный напиток
		// if (auth()->user()->can('change', $drink)) {
		//    return Redirect::Back();
		// }

		// Получение категорий 
		$categories = Drink::getCategories();
		// Получаем все имеющиеся игредиенты
		$ingredients =Ingredient::all();
		// Получаем все компоненты имеющиеся в данном напитке
		$components = Component::where('drink_id', $drink->id)->get();

		return view('drinks/change_drinks', ['drink' => $drink, 
			'categories' => $categories,
			'ingredients' => $ingredients,
			'components' => $components
		]);																		 									  						 
	}
	
	// Фуенкция изменяет поля напитка
	public function changeFields($drink_id, Request $request)
	{
        //Валидация данных
        $validatedData = $request->validate([
            'name' => 'required|min:5',
            'type' => 'required',
            'recipte' => 'required',
            'description' => 'required|max:250',
        ]);

		$drink = Drink::findOrFail($drink_id);
		$this->authorize('change', $drink);

		$drink->change(['name'=>$request->name, 
			'type'=>$request->type, 
			'recipte'=>$request->recipte, 
			'description'=>$request->description]);
		
		return response()->json(array('drink'=>$drink));
	}

	// Функция изменения компонеетов напитка 
	public function changeComponents($drink_id, Request $request)
	{
		//dd($request->all());
        //Валидация данных
        $validatedData = $request->validate([
        	'btnOptino' => 'required'
        ]);

        $drink = Drink::findOrFail($drink_id);
        $this->authorize('change', $drink);

		// Обработка выбранного пользователем действия
		switch ($request->btnOptino) {
			// Удаление компонента
			case 'btn-delete':
				$validatedData = $request->validate([
		        	'selected' => 'required'
		        ]);
				// Выбираем компоненты, данного напитка, из базы данных, которые соответствуют компонентам выбранным пользователем на странице
				$components = Component::whereIn('ingredient_id', $request->selected)->where('drink_id', $drink->id)->get();
				$mass = array('components' => $components);
        	    foreach ($components as $component) {
        	    	ComponentController::delete($component->id);
        	    }
        	    return response()->json($mass);
				break;
			// Изменение компонента
			case 'btn-change':
				$validatedData = $request->validate([
		            'amount.*' => 'required|regex:/^[1-9]{1}[0-9]{0,}/'
		        ]);
				$components = Component::where('drink_id', $drink->id)->get();
				$amounts = $request->get('amount');
        	    for ($i=0; $i < count($amounts); $i++) { 
        	    	ComponentController::changeAmount($components[$i]->id, $amounts[$i]);
        	    }
        	    break;
			default:
				//return $request->btmOption;
				break;
		}		
		return Redirect::Back();
	}

	// Функция добавления нового компонента к напитку
	public function addComponent($drink_id, Request $request)
	{
        //Валидация данных
        $validatedData = $request->validate([
            'amount' => 'required',
            'ingredient_id' => 'required',
            //'amount' => 'required|regex:/^[1-9]{1}[0-9]{1,}/',
        ]);
        $drink = Drink::findOrFail($drink_id);
        $this->authorize('change', $drink);
        
        $component = ComponentController::create($request->ingredient_id, $drink_id, $request->amount);

        return response()->json(array('ingredient' => $component->ingredient, 'component' => $component));
	}

    public function show($drink_id)
    {
        $drink = Drink::findOrFail($drink_id);
        $drink->countRating();
        $comments = array_reverse($drink->comments->all());
        $photos = $drink->photos->all();

        return view('drinks/drink_page', ['drink' => $drink, 
		   'comments' => $comments, 
		   'photos' => $photos]);
    }
}
