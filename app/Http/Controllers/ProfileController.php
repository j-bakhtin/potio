<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;

class ProfileController extends Controller
{	
	// Функция изменяет пользователя
    public function change(Request $request){
    	//Валидация данных
        $validatedData = $request->validate([
            'name' => 'required',
            'lastname' => 'required'
        ]);

    	$user = User::findOrFail($request->id);
		$user->change(['name'=>$request->name, 'lastname'=>$request->lastname]);

		return Redirect::back();
	}
}
