<?php

namespace App\Http\Controllers;
use Auth;
use App\Models\Comment;
use App\Models\Drink;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    // Функция создает коментарий
    public function create($drink_id, Request $request)
    {
        //Валидация данных
        $validatedData = $request->validate(['text' => 'required']);
        $drink = Drink::findOrFail($drink_id);

        $text = $request->text;
        $person_id = auth()->user()->id;
        
        $comment = new Comment(['user_id'=>$person_id, 
            'drink_id'=>$drink->id, 
            'text'=>$text]);

        $comment->save();

        $comment['created'] = $comment->created_at->diffForHumans();
        return response()->json(array('comment' => $comment, 'user' => $comment->user));
    }
}
