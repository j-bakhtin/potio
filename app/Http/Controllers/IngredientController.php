<?php

namespace App\Http\Controllers;

use App\Models\Ingredient;
use App\Models\Component;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class IngredientController extends Controller
{
    // Функция создания ингредиента
    public function create(Request $request)
    {
        // Валидация данных
        $validatedData = $request->validate([
           'name' => 'required|regex:/^[а-яА-я]{3,}/',
           'energy_value' => 'required|regex:/^[1-9]{1}[0-9]{0,}/',
        ]);

        //$this->authorize('create');

        $ingredient = new Ingredient($request->all());
        $ingredient->save();

        $request->session()->flash('success', 'Ингредиент добавлен');
        return Redirect::back();
    }

    // Функция отображает все ингредиенты
    public function showAll()
    {
        if(request()->has('search')){
            // Получаем запрос из поисковой строки
            $search = request()->get('search');
            // Получаем все наптки, на имена которых накладывается запрос
            $ingredients = Ingredient::where('name', 'like', '%'.$search.'%')->paginate(15);
        }else{
            $ingredients = Ingredient::paginate(15);
        }
        return view('ingredients/show_ingredients', ['ingredients' => $ingredients]);
    }

    // Функция отображает форму для изменени ингредиента
    public function showFormChange($ingredient_id)
    {   
        $ingredient = Ingredient::findOrFail($ingredient_id);
        return view('ingredients/change_ingredient', ['ingredient' => $ingredient]);
    }

    // Фуенкция изменяет поля напитка
    public function changeFields($ingredient_id, Request $request)
    {
        // Валидация данных
        $validatedData = $request->validate([
            'energy_value' => 'required|regex:/^[1-9]{1}[0-9]{0,}/',

        ]);

        $ingredient = Ingredient::findOrFail($ingredient_id);
        $ingredient->change(['energy_value'=>$request->energy_value]);
        return redirect()->action('IngredientController@showAll');
    }

    //Удаление ингредиента
    public function delete(Request $request)
    {
        // Валидация данных
        $validatedData = $request->validate(['selected' => 'required']);

        //Перебор выбранных напитков
        //Заменить на foreach
        foreach ($request->selected as $id) {
            $ingredient = Ingredient::findOrfail($id);
            $ingredient->delete();
        }

        //Фомирование пуш-сообщения 
        $message = 'Ингредиент удален';
        if (count($request->selected) > 1) {
            $message = 'Ингредиенты удалены';
        }
        
        $request->session()->flash('deleted', $message);
        return Redirect::back();
    }

    public function search($drink_id)
    {
        $components_id = Component::where('drink_id', $drink_id)->select('ingredient_id')->get();

        $search = request()->get('search');
        if(strlen($search) > 2)
            $ingredients = Ingredient::where('name', 'like', $search.'%')->get();
        else
            return response()->json(array('ingredients' => []));

        if(count($components_id) != 0){
            foreach ($components_id as $components) {
                $mass[] = $components->ingredient_id;
            }

            foreach ($ingredients as $ingredient) {
                if(in_array($ingredient->id, $mass)){
                    $ingredient['added'] = true;
                }
            }
        }

        return response()->json(array('ingredients' => $ingredients));
    }
}
