<?php

namespace App\Http\Controllers;

use App\Models\Image;
use App\Models\Drink;
use Illuminate\Http\Request;

class ImageController extends Controller
{
	// Функция соханяет изображеня напитка
	public static function save($files, $description, $drink_id){
		$drink = Drink::findOrFail($drink_id);

		// Пребор изображений
		foreach ($files as $file) {
			// Получаем имя изображения без расширения
	        $fileName = basename($file->getClientOriginalName(), '.'.$file->getClientOriginalExtension());
	        // Добавляем к имени изображения разницу между двумя датами в секундах
	        $fileName = $fileName.'_'.abs(strtotime('2019-08-06 00:00:00')-strtotime(date('Y-m-d H:i:s'))).'.'.$file->getClientOriginalExtension();
	        // Сохраняем изображение в папку public/uppload/images
	     	$file->move(public_path().'\uppload\images', $fileName);
	     	
	     	// Инициализируем поля для модели
	     	$data['src'] = 'uppload/images/'.$fileName;
	     	$data['description'] = $description;
	     	$data['drink_id'] = $drink->id;

	     	// Создаем и сохраняем модель в базу данных
	     	$image = new Image($data);
	     	$image->save();
        }
	}
}