<?php

namespace App\Http\Controllers;

use App\Models\Rating;
use Illuminate\Http\Request;

class RatingController extends Controller
{
	// Функция записывает в базу данных рейтинг напитка, оставленный пользователем
	// и обновляет информацию о средней оценке напитка на странице 
    public function update(Request $request){

        //Валидация данных
        $validatedData = $request->validate([
            'value' => 'required',
            'user_id' => 'required',
            'drink_id' => 'required'
        ]);

    	// Оценка оставленная пользователем
    	$value = $request->value;
    	$user_id = $request->user_id;
    	$drink_id = $request->drink_id;

    	// Получаем предыдуший рейтинг пользователя, если он есть
    	$rating_user = Rating::where('user_id', $user_id)->first();

    	// Проверям, был ли получен рейтинг
    	if($rating_user){
    		// Если да, то удаляем его
    		$rating_user->delete();
    	}

    	// Создаем новую запись в базе данных
    	$rating_user = new Rating(['rating'=>$value, 
		   'user_id'=>$user_id, 
		   'drink_id'=>$drink_id]);
    	$rating_user->save();

    	// Получаем из базы все записи с рейтингом данного напитка
    	$rating = Rating::where('drink_id', $drink_id)->get();

    	// Сумма всех рейтингов данного напитка
    	$rating_value = 0;
    	// Количество всех рейтингов данного напитка
    	$rating_count = 0;
        // Среднее значение рейтинга данного напитка
        $rating_average = 0;
        //Проверяем, есть ли рейтинги для данного напитка
        if(count($rating) > 0){
        	//Подсчет 
        	foreach ($rating as $item) {
        		$rating_value += $item->rating;
        		$rating_count +=1;
        	}
        	//Вычисление среднего значения
        	$rating_average = $rating_value / $rating_count;
        }
    	// Возвращаем среднее значение на страницу
		return response()->json(array('rating_average' => round($rating_average, 2),
                                      'rating_count' => $rating_count));
    }
}
