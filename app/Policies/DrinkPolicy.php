<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Drink;
use Illuminate\Auth\Access\HandlesAuthorization;

class DrinkPolicy
{
    use HandlesAuthorization;
    
    /**
     * Determine whether the user can view any drinks.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the drink.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Drink  $drink
     * @return mixed
     */
    public function view(User $user, Drink $drink)
    {
        //
    }

    /**
     * Determine whether the user can create drinks.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the drink.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Drink  $drink
     * @return bool
     */
    public function change(User $user, Drink $drink)
    {
        return $user->id == $drink->user_id;
    }

    /**
     * Determine whether the user can delete the drink.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Drink  $drink
     * @return bool
     */
    public function delete(User $user, Drink $drink)
    {
        return $user->id == $drink->user_id;
    }

    /**
     * Determine whether the user can restore the drink.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Drink  $drink
     * @return mixed
     */
    public function restore(User $user, Drink $drink)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the drink.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Drink  $drink
     * @return mixed
     */
    public function forceDelete(User $user, Drink $drink)
    {
        //
    }
}
