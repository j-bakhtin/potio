<?php

namespace App\Policies;

use App\Models\User;
use App\Ingredient;
use Illuminate\Auth\Access\HandlesAuthorization;

class IngredientPolicy
{
    use HandlesAuthorization;
    
    /**
     * Determine whether the user can view any ingredients.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the ingredient.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Ingredient  $ingredient
     * @return mixed
     */
    public function view(User $user, Ingredient $ingredient)
    {
        //
    }

    /**
     * Determine whether the user can create ingredients.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->status == 'admin';
    }

    /**
     * Determine whether the user can update the ingredient.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Ingredient  $ingredient
     * @return mixed
     */
    public function update(User $user, Ingredient $ingredient)
    {
        //
    }

    /**
     * Determine whether the user can delete the ingredient.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Ingredient  $ingredient
     * @return mixed
     */
    public function delete(User $user, Ingredient $ingredient)
    {
        //
    }

    /**
     * Determine whether the user can restore the ingredient.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Ingredient  $ingredient
     * @return mixed
     */
    public function restore(User $user, Ingredient $ingredient)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the ingredient.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Ingredient  $ingredient
     * @return mixed
     */
    public function forceDelete(User $user, Ingredient $ingredient)
    {
        //
    }
}
