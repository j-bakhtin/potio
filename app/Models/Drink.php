<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Drink extends Model
{
    protected $table = 'drinks';

    protected $fillable = ['name', 'type', 'recipte', 'description', 'user_id'];

    // Каьегории напитков
    protected static $categories = [
    	'Чай',
    	'Алкоголь',
    	'Сок',
    	'Кофе',
    	'Смузи',
    	'Компот',
    	'Йогурт',
    	'Лимонад'
    ];

    public function change($data)
    {
        // Данные проверяютяс в контроллере
        $this->name = $data['name'];
        $this->type = $data['type'];
        $this->recipte = $data['recipte'];
        $this->description = $data['description'];
        $this->save();
        return;
    }

    public static function getCategories()
	{
		return self::$categories; 
	}

    public function countRating(){
        // Сумма всех рейтингов для данного напитка
        $rating_summ = 0;
        // Количество всех рейтингов
        $rating_count = 0;
        // Рейтинг пользователя просматривающего напиток
        $rating_user = 0;
        // Среднее значениерейтинга
        $rating_average = 0;
        // Подсчитываем для каждого напитка
        if(count($this->rating) > 0){

            foreach ($this->rating as $item) {
                $rating_summ += $item->rating;
                $rating_count++;
                if(auth()->check()){
                    if($item->user_id == auth()->user()->id){
                        $rating_user = $item->rating;
                    }
                }
            }
            //Вычисление среднего значения
            $this['rating_average'] = round($rating_average = $rating_summ / $rating_count, 2);
        } else 
            $this['rating_average'] = 0;
        
        $this['rating_user'] = $rating_user;
        $this['rating_count'] = $rating_count;

        return;
    }

    public function countEnergyValue(){
        $energy_value_sum = 0;
        foreach ($this->components as $component) {
            $ingredient = $component->ingredient;
            $energy_value_sum += $component->amount * $ingredient->energy_value;
        }
        $this['energy_value_sum'] = $energy_value_sum;
        return;
    }

	public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function photos(){
        return $this->hasMany(Image::class);
    }

    public function components()
    {
        return $this->hasMany(Component::class);
    }

    public function user()
    {
        return $this->hasOne(User::class);
    }

    public function rating(){
        return $this->hasMany(Rating::class);
    } 
}
