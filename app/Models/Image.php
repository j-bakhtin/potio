<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
	protected $table = 'photos';
    protected $fillable = ['drink_id', 'src', 'description'];
}
