<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
    protected $table = 'ingredients';
    protected $fillable = ['name', 'energy_value'];

    public function components()
    {
        return $this->hasMany('App\Models\Component', 'ingredient_id');
    }

    public function change($data)
    {
        $this->energy_value = $data['energy_value'];
        $this->save();
    }
}
