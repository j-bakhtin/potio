<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
	// База данных
    protected $table = 'comments';

    // Автозаполнение
    protected $fillable = ['user_id', 'drink_id', 'text'];

    // Зависимости
    public function user()
	{
		return $this->belongsTo(User::class);
	}
}
