<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Component extends Model
{
	protected $table = 'components';
	protected $fillable = ['ingredient_id', 'drink_id', 'amount'];

	public function ingredient()
	{
		return $this->belongsTo('App\Models\Ingredient');
	}
}
