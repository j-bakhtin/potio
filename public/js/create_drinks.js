$(document).ready(function () {

	$('#search-input').keypress(function (element){
        // console.log(element.target);
        var search = $('#search-input')[0].value;
        console.log(search);
        $.get('/create_drinks/search',{search:search}, function(data) {
            console.log(data);

            $( ".add-ingredients tbody" ).empty();
            data.ingredients.forEach(function(item, i) {
                console.log(item);
                var tr = $("<tr class='tr_"+ item.id +"'></tr>");

                tr.append("<td class='name'><p>"+ item.name +"</p></td>");
                tr.append("<td class='energy_value'><p>"+ item.energy_value +"</p></td>");
                if(item.added != true){
                    tr.append("<td><input id='amount_"+ item.id +"' min='1' type='number' value='1'></td>");
                    tr.append("<td><button type='button' class='btn btn-primary add-btn' value='"+ item.id +"'>Добавить</button></td>");
                }else{
                    tr.append("<td><input name='amount_"+ item.id +"' id='amount_"+ item.id +"' type='number' value='1' disabled></td>");
                    tr.append("<td>Добавлено</td>");
                }

                $(".add-ingredients tbody").append(tr);
            });
        });
    });

    $(document).on('click', '.add-btn', function (element){
        var ingredient_id = element.target.value;
        var name = $('.add-ingredients .tr_'+ingredient_id +' .name p').text();
        var energy_value = $('.add-ingredients .tr_'+ingredient_id +' .energy_value p').text();
        var ingredient_amount = $('.add-ingredients #amount_' + ingredient_id)[0].value;

        console.log(ingredient_id);
        console.log(ingredient_amount);
        console.log(name);
        console.log(energy_value);
        
        var tr = $("<tr class='tr_"+ingredient_id+"'></tr>");

        tr.append("<td>"+ name +"</td>");
        tr.append("<td>"+ energy_value +"</td>");
        tr.append("<td><input name='ingredient_id[]' type='hidden' min='1' value='"+ ingredient_id +"'><input class='amount' name='amount[]' type='number' min='1' value='"+ ingredient_amount +"'></td>");
        tr.append("<td><label><input class='selected' name='selected[]' type='checkbox' value='"+ ingredient_id +"'> Выбрать</label></td>");

        $(".change-delete-ingredients tbody").append(tr);
        
        // Доделать
        var tr_change = $(".add-ingredients tbody .tr_" + ingredient_id);
        tr_change.empty();

        tr_change.append("<td>"+ name +"</td>");
        tr_change.append("<td>"+ energy_value +"</td>");
        tr_change.append("<td><input id='amount_"+ ingredient_id +"' type='number' value='1' disabled></td>");
        tr_change.append("<td>Добавлено</td>");
    });

});