var user_id;
var drink_id;
function setUserParam(_user_id, _drink_id){
    user_id = _user_id;
    drink_id = _drink_id;
    console.log(user_id);
    console.log(drink_id);
}

function getCheckedCheckBoxes(name) {
  var checkboxes = document.getElementsByClassName(name);
  var checkboxesChecked = []; // можно в массиве их хранить, если нужно использовать 
  for (var index = 0; index < checkboxes.length; index++) {
     if (checkboxes[index].checked) {
        checkboxesChecked.push(checkboxes[index].value); // положим в массив выбранный
        // alert(checkboxes[index].value); // делайте что нужно - это для наглядности
     }
  }
  return checkboxesChecked; // для использования в нужном месте
}

function getAmount(name) {
  var mass = document.getElementsByClassName(name);
  var mass_amount = []; // можно в массиве их хранить, если нужно использовать 
  for (var index = 0; index < mass.length; index++) {
    mass_amount.push(mass[index].value); // положим в массив выбранный
    // alert(checkboxes[index].value); // делайте что нужно - это для наглядности
  }
  return mass_amount; // для использования в нужном месте
}

$(document).ready(function() {
    $.ajaxSetup({
        headers: {
        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#search-input').keypress(function (element){
        // console.log(element.target);
        var search = $('#search-input')[0].value;
        console.log(search);
        $.get('/drinks/' + drink_id + '/change/search',{search:search}, function(data) {
            console.log(data);

            $( ".add-ingredients tbody" ).empty();
            data.ingredients.forEach(function(item, i) {
                console.log(item);
                var tr = $("<tr class='tr_"+ item.id +"'></tr>");

                tr.append("<td>"+ item.name +"</td>");
                tr.append("<td>"+ item.energy_value +"</td>");
                if(item.added != true){
                    tr.append("<td><input name='amount_"+ item.id +"' id='amount_"+ item.id +"' min='1' type='number' value='1'></td>");
                    tr.append("<td><button class='btn btn-primary add-btn' value='"+ item.id +"'>Добавить</button></td>");
                }else{
                    tr.append("<td><input name='amount_"+ item.id +"' id='amount_"+ item.id +"' type='number' value='1' disabled></td>");
                    tr.append("<td>Добавлено</td>");
                }

                $(".add-ingredients tbody").append(tr);
            });
        });
    });

    $(document).on('click', '.add-btn', function (element){
        var ingredient_id = element.target.value;
        var ingredient_amount = $('#amount_' + ingredient_id)[0].value;
        console.log(ingredient_id);
        console.log(ingredient_amount);
        $.get('/drinks/'+ drink_id +'/AddComponent',{ingredient_id:ingredient_id, amount:ingredient_amount}, function(data) {
            console.log(data);
            var tr = $("<tr class='tr_"+data.ingredient.id+"'></tr>");

            tr.append("<td>"+ data.ingredient.name +"</td>");
            tr.append("<td>"+ data.ingredient.energy_value +"</td>");
            tr.append("<td><input class='amount' name='amount[]' type='number' min='1' value='"+ data.component.amount +"'></td>");
            tr.append("<td><label><input class='selected' name='selected[]' type='checkbox' value='"+ data.ingredient.id +"'> Выбрать</label></td>");

            $(".change-delete-ingredients tbody").append(tr);
            
            // Доделать
            var tr_change = $(".add-ingredients tbody .tr_" + data.ingredient.id);
            tr_change.empty();

            tr_change.append("<td>"+ data.ingredient.name +"</td>");
            tr_change.append("<td>"+ data.ingredient.energy_value +"</td>");
            tr_change.append("<td><input name='amount_"+ data.ingredient.id +"' id='amount_"+ data.ingredient.id +"' type='number' value='1' disabled></td>");
            tr_change.append("<td>Добавлено</td>");
        });
    });

    $(document).on('click', '#btn-delete', function (element){
        var checkedCheckboxs = getCheckedCheckBoxes('selected');
        var btnOptino = 'btn-delete';
        $.get('/drinks/'+ drink_id +'/changeComponents',{selected:checkedCheckboxs, btnOptino:btnOptino}, function(data) {
            console.log(data);
            data.components.forEach(function(item, i) {
                console.log(item);
                var tr = $(".change-delete-ingredients tbody .tr_"+item.ingredient_id).remove();
                // $(".change-delete-ingredients tbody").remove(tr);
            });
        });
    });

    $(document).on('click', '#btn-change', function (element){
        var amount = getAmount('amount');
        var btnOptino = 'btn-change';
        $.get('/drinks/'+ drink_id +'/changeComponents',{amount:amount, btnOptino:btnOptino}, function(data) {
            $(".change-delete-ingredients-status").text('Изменения приняты');
            setTimeout(function(){
                $(".change-delete-ingredients-status").empty();
            }, 1000);
            
        });
    });

    $(document).on('click', '#btn-change-fields', function (element){
        var name = $('#name')[0].value;
        var type = $('#type')[0].value;
        var recipte = $('#recipte')[0].value;
        var description = $('#description')[0].value;
        console.log(name);
        console.log(type);
        console.log(recipte);
        console.log(description);
        $.get('/drinks/'+ drink_id +'/changeFields',{name:name, type:type, recipte:recipte, description:description}, function(data) {
            console.log(data);
            $(".change-fields-status").text('Изменения приняты');
            setTimeout(function(){
                $(".change-fields-status").empty();
            }, 1000);
            
        });
    });
});