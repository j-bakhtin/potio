
function setParam(_user_id, _drink_id){
    user_id = _user_id;
    drink_id = _drink_id;
    console.log(user_id);
    console.log(drink_id);
}

function diffForHumans(unixTime, ms) {
  
  // Adjust for milliseconds
  ms = ms || false;
  unixTime = (ms) ? unixTime * 1000 : unixTime;

  var d = new Date();
  var diff = Math.abs(d.getTime() - unixTime);
  var intervals = {
    y: diff / (365 * 24 * 60 * 60 * 1 * 1000),
    m: diff / (30.5 * 24 * 60 * 60 * 1 * 1000),
    d: diff / (24 * 60 * 60 * 1 * 1000),
    h: diff / (60 * 60 * 1 * 1000),
    i: diff / (60 * 1 * 1000),
    s: diff / (1 * 1000),
  }

  Object.keys(intervals).map(function(value, index) {
    return intervals[value] = Math.floor(intervals[value]);
  })

  var unit;
  var count;

  switch (true) {
    case intervals.y > 0:
      count = intervals.y;
      unit = 'year';
      break;
    case intervals.m > 0:
      count = intervals.m;
      unit = 'month';
      break;
    case intervals.d > 0:
      count = intervals.d;
      unit = 'day';
      break;
    case intervals.h > 0:
      count = intervals.h;
      unit = 'hour';
      break;
    case intervals.i > 0:
      count = intervals.i;
      unit = 'minute';
      break;
    default:
      count = intervals.s;
      unit = 'second';
      break;

  }

  if(count > 1) {
    unit = unit + 's';
  }

  if(count === 0) {
    return 'now';
  }

  return count + ' ' + unit + ((unixTime > d.getTime()) ? ' from now' : ' ago');
}

 $(document).ready(function() {

    $.ajaxSetup({
        headers: {
        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.input').click(function(element){
        // console.log(element.target);

        var user_id = $('#user_id')[0].value;
        var drink_id = $('#drink_id')[0].value;
        var value = element.target.value;

        for (var i = 1; i <= 5; i++) {
            if (i <= value) {
                // console.log($('#input'+i));
                $('#input'+i)[0].checked = true;
            } 
            else { 
                // console.log($('#input'+i));
                $('#input'+i)[0].checked = false;
            }
        }
        
        $.get('/rating',{value:value, user_id:user_id, drink_id:drink_id}, function(data) {
            $( ".rating-container .average" ).text(data.rating_average);
            $( ".rating-container .count" ).text(data.rating_count);
        });
    });

    $('#add-comment').click(function(element){
        var text = $('#text-comment')[0].value;
        var not_comment = $('.comments .not-comment');
        if(not_comment.text().length > 0){
          $('.comments').empty();
        }
        if (text.length != 0){
            $.get("/drinks/"+ drink_id +"/comment", {text:text, user_id:user_id}, function(data){
                console.log(data);

                var comment = $("<div class='card comment'></div>");
                comment.append("<div class='card-header'>"+ data.user.name +"</div>");
                comment.append("<div class='card-body'>"+ data.comment.text +"</div>");
                comment.append("<div class='card-footer'>"+ data.comment.created +"</div>");
                $('.comments').prepend('<br>');
                $('.comments').prepend(comment);
            });
          }
    });
});