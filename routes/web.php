<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Главная страница сайта
Route::get('/', 'DrinkController@showAll');
//Отображение конкретного напитка
Route::get('/drinks/{id}', 'DrinkController@show');

// Группа доступная для авторизованных пользователей
Route::group(['middleware'=>'auth'], function(){
	// === Напитки ===
	//Страница создания ингредиента
	Route::get('/create_drinks', 'DrinkController@showFormCreate');
	//Страница с отображением всех ингредиентов
	Route::get('/my_drinks', 'DrinkController@showMy')->name('ShowDrinks');
	//Страница изменения напитка
	Route::get('/drinks/{drink}/change', 'DrinkController@showFormChange');
	//рейтинг напитка
	Route::get('/rating', 'RatingController@update')->name("ratingDrink");
	//Обработка отправленной формы для создания ингредиета
	Route::post('/create_drinks', 'DrinkController@create')->name('CreateDrinks');
	//Удаление напитка
	Route::post('/drinks/delete', 'DrinkController@delete')->name("DeleteDrink");
	// Изменение полей напитка
	Route::get('/drinks/{id}/changeComponents', 'DrinkController@changeComponents');
	// Изменение/Удаление компонентов напитка
	Route::get('/drinks/{id}/changeFields', 'DrinkController@changeFields');
	// Добавление компанентов напитка
	Route::get('/drinks/{id}/AddComponent', 'DrinkController@addComponent')->name("AddComponentsDrink");
	//Добавление комментария под определенный пост
	Route::get('/drinks/{id}/comment', 'CommentController@create');

	Route::get('/drinks/{drink}/change/search', 'IngredientController@search');

	// === Профиль ===
	//Отображение профиля авторизированного пользователя
	Route::get('/myProfile',  function(){
        return view('profile/show_profile', ['user' => auth()->user()]);
    });
	Route::get('/myProfile/change',  function(){
    	return view('profile/show_change_profile', ['user' => auth()->user()]);
    })->name('showChangeMyProfile');
	Route::post('/myProfile/changed',  'ProfileController@change')->name('changedMyProfile');
	Route::get('/myProfile/delete',  'ProfileController@showFormChange')->name('deleteMyProfile');

	// === Ингредиенты ===
	//Страница создания ингредиента
	Route::group(['middleware'=>'checkStatus'], function(){
		//Страница создания ингредиента
		Route::get('/create_ingredients', function () {
		    return view('ingredients/create_ingredient');
		});

		//Обработка отправленной формы для создания ингредиета
		Route::post('/create_ingredients', 'IngredientController@create')->name('CreateIngredient');
		Route::get('/ingredients/{id}/change', 'IngredientController@showFormChange');
		//Удаление ингредиента
		Route::get('/ingredients/delete', 'IngredientController@delete')->name("DeleteIngredient");
		// Изменение/Удаление компонентов напитка
		Route::post('/ingredients/{id}/changeFields', 'IngredientController@changeFields')->name("changeFieldsIngredient");
	});
	//Страница с отображением всех ингредиентов
	Route::get('/show_ingredients', 'IngredientController@showAll')->name('ShowIngredients'); 
});

Auth::routes();
